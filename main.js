/*
 * Copyright (c) 2021 EdgerOS Team.
 * All rights reserved.
 *
 * Detailed license information can be found in the LICENSE file.
 *
 * File: main.js.
 *
 * Author: hanhui@acoinfo.com
 *
 */

/* Import system modules */
const WebApp = require('webapp');
const io = require('socket.io');

/* Import routers */
const myrouter = require('./routers/rest');

/* Create App */
const app = WebApp.createApp();

/* Set static path */
app.use(WebApp.static('./public'));

/* Set test rest */
app.use('/api', myrouter);

/* Rend test */
app.get('/temp.html', function(req, res) {
	res.render('temp', { time: Date.now() });
});

/* Start App */
app.start();
console.log('ports:', sys.appinfo().ports)
console.log('ports:', sys.appinfo().appid)
console.log('ports:', sys.appinfo().port)

const socketio = io(app, {
  serveClient: false,
  pingInterval: 10000,
  pingTimeout: 5000
})

socketio.on('connection', sockio => {
  console.log('>> socket connected <<')
  sockio.emit('falcon', 'reply please')

  sockio.on('message', (data, cb) => {
    console.log('recive:', data)
    // callback(`>> server: ${msg}`)
    cb('endddddd')
  })
})


/* Event loop */
require('iosched').forever();
