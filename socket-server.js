const io = require('socket.io');

class SocketServer {
  instance = null
  constructor(app) {
    this.instance = io(app, {
      serveClient: false,
      pingInterval: 10000,
      pingTimeout: 5000
    })
  }
}

export const ins = new SocketServer()
